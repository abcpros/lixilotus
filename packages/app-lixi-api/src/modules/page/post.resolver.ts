import {
  Account,
  CreatePostInput,
  Page,
  PaginationArgs,
  Post,
  PostConnection,
  PostOrder,
  RepostInput,
  Token,
  UpdatePostInput
} from '@bcpros/lixi-models';
import { NotificationLevel } from '@bcpros/lixi-prisma';
import BCHJS from '@bcpros/xpi-js';
import { findManyCursorConnection } from '@devoxa/prisma-relay-cursor-connection';
import { Inject, Injectable, Logger, UseFilters, UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Parent, Query, ResolveField, Resolver, Subscription } from '@nestjs/graphql';
import { SkipThrottle, Throttle } from '@nestjs/throttler';
import { ChronikClient } from 'chronik-client';
import { PubSub } from 'graphql-subscriptions';
import * as _ from 'lodash';
import { I18n, I18nService } from 'nestjs-i18n';
import { InjectChronikClient } from 'src/common/modules/chronik/chronik.decorators';
import { NOTIFICATION_TYPES } from 'src/common/modules/notifications/notification.constants';
import { NotificationService } from 'src/common/modules/notifications/notification.service';
import PostResponse from 'src/common/post.response';
import { PostAccountEntity } from 'src/decorators/postAccount.decorator';
import { GqlHttpExceptionFilter } from 'src/middlewares/gql.exception.filter';
import VError from 'verror';
import { connectionFromArraySlice } from '../../common/custom-graphql-relay/arrayConnection';
import ConnectionArgs, { getPagingParameters } from '../../common/custom-graphql-relay/connection.args';
import { GqlJwtAuthGuard, GqlJwtAuthGuardByPass } from '../auth/guards/gql-jwtauth.guard';
import { HashtagService } from '../hashtag/hashtag.service';
import { PrismaService } from '../prisma/prisma.service';
import { HASHTAG, POSTS } from './constants/meili.constants';
import { MeiliService } from './meili.service';
import { GqlThrottlerGuard } from '../auth/guards/gql-throttler.guard';

const pubSub = new PubSub();

@Injectable()
@Resolver(() => Post)
@UseFilters(GqlHttpExceptionFilter)
export class PostResolver {
  private logger: Logger = new Logger(this.constructor.name);

  constructor(
    private prisma: PrismaService,
    private meiliService: MeiliService,
    private readonly notificationService: NotificationService,
    private hashtagService: HashtagService,
    @Inject('xpijs') private XPI: BCHJS,
    @InjectChronikClient('xpi') private chronik: ChronikClient,
    @I18n() private i18n: I18nService
  ) {}

  @Subscription(() => Post)
  postCreated() {
    return pubSub.asyncIterator('postCreated');
  }

  @SkipThrottle()
  @Query(() => Post)
  async post(@Args('id', { type: () => String }) id: string) {
    return this.prisma.post.findUnique({
      where: { id: id },
      include: { page: true }
    });
  }

  @SkipThrottle()
  @Query(() => PostConnection)
  @UseGuards(GqlJwtAuthGuardByPass)
  async allPosts(
    @PostAccountEntity() account: Account,
    @Args() { after, before, first, last, minBurnFilter }: PaginationArgs,
    @Args() args: ConnectionArgs,
    @Args({ name: 'accountId', type: () => Number, nullable: true }) accountId: number,
    @Args({
      name: 'orderBy',
      type: () => [PostOrder!],
      nullable: true
    })
    orderBy: PostOrder[]
  ) {
    let result;

    if (account) {
      if (accountId && !_.isNil(account) && accountId !== account.id) {
        const invalidAccountMessage = await this.i18n.t('account.messages.invalidAccount');
        throw new VError(invalidAccountMessage);
      }

      const followingsAccount = await this.prisma.followAccount.findMany({
        where: { followerAccountId: account.id },
        select: { followingAccountId: true }
      });
      const listFollowingsAccountIds = followingsAccount.map(item => item.followingAccountId);

      const followingPagesAccount = await this.prisma.followPage.findMany({
        where: { accountId: account.id },
        select: { pageId: true }
      });
      const listFollowingsPageIds = followingPagesAccount.map(item => item.pageId);

      result = await findManyCursorConnection(
        args =>
          this.prisma.post.findMany({
            include: {
              postAccount: true,
              comments: true
            },
            where: {
              OR: [
                {
                  lotusBurnScore: {
                    gte: minBurnFilter ?? 0
                  }
                },
                {
                  postAccount: {
                    id: account.id
                  }
                },
                {
                  AND: [
                    {
                      postAccount: {
                        id: { in: listFollowingsAccountIds }
                      }
                    },
                    {
                      lotusBurnScore: {
                        gte: 0
                      }
                    }
                  ]
                },
                {
                  AND: [
                    { pageId: { in: listFollowingsPageIds } },
                    {
                      lotusBurnScore: {
                        gte: 1
                      }
                    }
                  ]
                }
              ]
            },
            orderBy: orderBy ? orderBy.map(item => ({ [item.field]: item.direction })) : undefined,
            ...args
          }),
        () =>
          this.prisma.post.count({
            where: {
              OR: [
                {
                  lotusBurnScore: {
                    gte: minBurnFilter ?? 0
                  }
                },
                {
                  postAccount: {
                    id: account.id
                  }
                },
                {
                  AND: [
                    {
                      postAccount: {
                        id: { in: listFollowingsAccountIds }
                      }
                    },
                    {
                      lotusBurnScore: {
                        gte: 0
                      }
                    }
                  ]
                },
                {
                  AND: [
                    { pageId: { in: listFollowingsPageIds } },
                    {
                      lotusBurnScore: {
                        gte: 1
                      }
                    }
                  ]
                },
                {
                  AND: [
                    { pageId: { in: listFollowingsPageIds } },
                    {
                      lotusBurnScore: {
                        gte: 1
                      }
                    }
                  ]
                }
              ]
            }
          }),
        { first, last, before, after }
      );
    } else {
      result = await findManyCursorConnection(
        args =>
          this.prisma.post.findMany({
            include: { postAccount: true, comments: true },
            where: {
              lotusBurnScore: {
                gte: minBurnFilter ?? 0
              }
            },
            orderBy: orderBy ? orderBy.map(item => ({ [item.field]: item.direction })) : undefined,
            ...args
          }),
        () =>
          this.prisma.post.count({
            where: {
              lotusBurnScore: {
                gte: minBurnFilter ?? 0
              }
            }
          }),
        { first, last, before, after }
      );
    }
    return result;
  }

  @SkipThrottle()
  @Query(() => PostConnection)
  @UseGuards(GqlJwtAuthGuardByPass)
  async allOrphanPosts(
    @PostAccountEntity() account: Account,
    @Args() { after, before, first, last, minBurnFilter }: PaginationArgs,
    @Args({ name: 'query', type: () => String, nullable: true })
    accountId: number,
    @Args({
      name: 'orderBy',
      type: () => PostOrder,
      nullable: true
    })
    orderBy: PostOrder
  ) {
    if (accountId && !_.isNil(account) && accountId !== account.id) {
      const invalidAccountMessage = await this.i18n.t('account.messages.invalidAccount');
      throw new VError(invalidAccountMessage);
    }

    const result = await findManyCursorConnection(
      args =>
        this.prisma.post.findMany({
          include: { postAccount: true, comments: true },
          where: {
            OR: [
              { postAccountId: accountId },
              {
                AND: [
                  {
                    page: null
                  },
                  {
                    token: null
                  },
                  {
                    lotusBurnScore: {
                      gte: minBurnFilter ?? 0
                    }
                  }
                ]
              }
            ]
          },
          orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined,
          ...args
        }),
      () =>
        this.prisma.post.count({
          where: {
            OR: [
              { postAccountId: accountId },
              {
                AND: [
                  {
                    page: null
                  },
                  {
                    token: null
                  },
                  {
                    lotusBurnScore: {
                      gte: minBurnFilter ?? 0
                    }
                  }
                ]
              }
            ]
          }
        }),
      { first, last, before, after }
    );
    return result;
  }

  @SkipThrottle()
  @Query(() => PostConnection)
  @UseGuards(GqlJwtAuthGuard)
  async allPostsByPageId(
    @PostAccountEntity() account: Account,
    @Args() { after, before, first, last, minBurnFilter }: PaginationArgs,
    @Args({ name: 'id', type: () => String, nullable: true })
    id: string,
    @Args({
      name: 'orderBy',
      type: () => [PostOrder!],
      nullable: true
    })
    orderBy: PostOrder[]
  ) {
    let result;
    const page = await this.prisma.page.findFirst({
      where: {
        id: id
      }
    });

    if (account.id === page?.pageAccountId) {
      result = await findManyCursorConnection(
        args =>
          this.prisma.post.findMany({
            include: { postAccount: true, comments: true, reposts: { select: { account: true, accountId: true } } },
            where: {
              OR: [
                {
                  AND: [{ postAccountId: account.id }, { pageId: id }]
                },
                {
                  AND: [{ pageId: id }, { lotusBurnScore: { gte: minBurnFilter ?? 0 } }]
                }
              ]
            },
            orderBy: orderBy ? orderBy.map(item => ({ [item.field]: item.direction })) : undefined,
            ...args
          }),
        () =>
          this.prisma.post.count({
            where: {
              OR: [
                {
                  AND: [{ postAccountId: account.id }, { pageId: id }]
                },
                {
                  AND: [{ pageId: id }, { lotusBurnScore: { gte: minBurnFilter ?? 0 } }]
                }
              ]
            }
          }),
        { first, last, before, after }
      );
    } else {
      result = await findManyCursorConnection(
        args =>
          this.prisma.post.findMany({
            include: { postAccount: true, comments: true, reposts: { select: { account: true, accountId: true } } },
            where: {
              OR: [
                {
                  AND: [{ postAccountId: account.id }, { pageId: id }]
                },
                {
                  AND: [{ pageId: id }, { lotusBurnScore: { gte: minBurnFilter ?? 0 } }]
                }
              ]
            },
            orderBy: orderBy ? orderBy.map(item => ({ [item.field]: item.direction })) : undefined,
            ...args
          }),
        () =>
          this.prisma.post.count({
            where: {
              OR: [
                {
                  AND: [{ postAccountId: account.id }, { pageId: id }]
                },
                {
                  AND: [{ pageId: id }, { lotusBurnScore: { gte: minBurnFilter ?? 0 } }]
                }
              ]
            }
          }),
        { first, last, before, after }
      );
    }

    return result;
  }

  @SkipThrottle()
  @Query(() => PostResponse, { name: 'allPostsBySearch' })
  async allPostsBySearch(
    @Args() args: ConnectionArgs,
    @Args({ name: 'query', type: () => String, nullable: true })
    @Args({ name: 'minBurnFilter', type: () => Int, nullable: true })
    query: string
  ): Promise<PostResponse> {
    const { limit, offset } = getPagingParameters(args);

    const count = await this.meiliService.searchByQueryEstimatedTotalHits(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query
    );

    const posts = await this.meiliService.searchByQueryHits(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query,
      offset!,
      limit!
    );

    const postsId = _.map(posts, 'id');

    const searchPosts = await this.prisma.post.findMany({
      where: {
        id: { in: postsId }
      }
    });

    return connectionFromArraySlice(searchPosts, args, {
      arrayLength: count || 0,
      sliceStart: offset || 0
    });
  }

  @SkipThrottle()
  @Query(() => PostResponse, { name: 'allPostsBySearchWithHashtag' })
  async allPostsBySearchWithHashtag(
    @Args({ name: 'minBurnFilter', type: () => Int, nullable: true })
    minBurnFilter: number,
    @Args()
    args: ConnectionArgs,
    @Args({ name: 'query', type: () => String, nullable: true })
    query: string,
    @Args({ name: 'hashtags', type: () => [String], nullable: true })
    hashtags: string[],
    @Args({
      name: 'orderBy',
      type: () => PostOrder,
      nullable: true
    })
    orderBy: PostOrder
  ): Promise<PostResponse> {
    const { limit, offset } = getPagingParameters(args);

    const count = await this.hashtagService.searchByQueryEstimatedTotalHits(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query,
      hashtags
    );

    const posts = await this.hashtagService.searchByQueryHits(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query,
      hashtags,
      offset!,
      limit!
    );

    const postsId = _.map(posts, 'id');

    const searchPosts = await this.prisma.post.findMany({
      where: {
        AND: [
          {
            id: { in: postsId }
          },
          {
            lotusBurnScore: {
              gte: minBurnFilter ?? 0
            }
          }
        ]
      },
      orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined
    });

    return connectionFromArraySlice(searchPosts, args, {
      arrayLength: count || 0,
      sliceStart: offset || 0
    });
  }

  @SkipThrottle()
  @Query(() => PostResponse, { name: 'allPostsBySearchWithHashtagAtPage' })
  async allPostsBySearchWithHashtagAtPage(
    @Args({ name: 'minBurnFilter', type: () => Int, nullable: true })
    minBurnFilter: number,
    @Args()
    args: ConnectionArgs,
    @Args({ name: 'query', type: () => String, nullable: true })
    query: string,
    @Args({ name: 'hashtags', type: () => [String], nullable: true })
    hashtags: string[],
    @Args({ name: 'pageId', type: () => String, nullable: true })
    pageId: string,
    @Args({
      name: 'orderBy',
      type: () => PostOrder,
      nullable: true
    })
    orderBy: PostOrder
  ) {
    const { limit, offset } = getPagingParameters(args);

    const count = await this.hashtagService.searchByQueryEstimatedTotalHitsAtPage(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query,
      hashtags,
      pageId
    );

    const posts = await this.hashtagService.searchByQueryHitsAtPage(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query,
      hashtags,
      pageId,
      offset!,
      limit!
    );

    const postsId = _.map(posts, 'id');

    const searchPosts = await this.prisma.post.findMany({
      where: {
        AND: [
          {
            id: { in: postsId }
          },
          {
            lotusBurnScore: {
              gte: minBurnFilter ?? 0
            }
          }
        ]
      },
      orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined
    });

    return connectionFromArraySlice(searchPosts, args, {
      arrayLength: count || 0,
      sliceStart: offset || 0
    });
  }

  @SkipThrottle()
  @Query(() => PostResponse, { name: 'allPostsBySearchWithHashtagAtToken' })
  async allPostsBySearchWithHashtagAtToken(
    @Args({ name: 'minBurnFilter', type: () => Int, nullable: true })
    minBurnFilter: number,
    @Args()
    args: ConnectionArgs,
    @Args({ name: 'query', type: () => String, nullable: true })
    query: string,
    @Args({ name: 'hashtags', type: () => [String], nullable: true })
    hashtags: string[],
    @Args({ name: 'tokenId', type: () => String, nullable: true })
    tokenId: string,
    @Args({
      name: 'orderBy',
      type: () => PostOrder,
      nullable: true
    })
    orderBy: PostOrder
  ) {
    const { limit, offset } = getPagingParameters(args);

    const count = await this.hashtagService.searchByQueryEstimatedTotalHitsAtToken(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query,
      hashtags,
      tokenId
    );

    const posts = await this.hashtagService.searchByQueryHitsAtToken(
      `${process.env.MEILISEARCH_BUCKET}_${POSTS}`,
      query,
      hashtags,
      tokenId,
      offset!,
      limit!
    );

    const postsId = _.map(posts, 'id');

    const searchPosts = await this.prisma.post.findMany({
      where: {
        AND: [
          {
            id: { in: postsId }
          },
          {
            lotusBurnScore: {
              gte: minBurnFilter ?? 0
            }
          }
        ]
      },
      orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined
    });

    return connectionFromArraySlice(searchPosts, args, {
      arrayLength: count || 0,
      sliceStart: offset || 0
    });
  }

  @SkipThrottle()
  @Query(() => PostConnection)
  @UseGuards(GqlJwtAuthGuard)
  async allPostsByTokenId(
    @PostAccountEntity() account: Account,
    @Args() { after, before, first, last, minBurnFilter }: PaginationArgs,
    @Args({ name: 'id', type: () => String, nullable: true })
    id: string,
    @Args({
      name: 'orderBy',
      type: () => PostOrder,
      nullable: true
    })
    orderBy: PostOrder
  ) {
    const result = await findManyCursorConnection(
      args =>
        this.prisma.post.findMany({
          include: { postAccount: true, comments: true },
          where: {
            OR: [
              {
                AND: [{ postAccountId: account.id }, { tokenId: id }]
              },
              {
                AND: [
                  {
                    tokenId: id
                  },
                  {
                    lotusBurnScore: {
                      gte: minBurnFilter ?? 0
                    }
                  }
                ]
              }
            ]
          },
          orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined,
          ...args
        }),
      () =>
        this.prisma.post.count({
          where: {
            OR: [
              {
                AND: [{ postAccountId: account.id }, { tokenId: id }]
              },
              {
                AND: [
                  {
                    tokenId: id
                  },
                  {
                    lotusBurnScore: {
                      gte: minBurnFilter ?? 0
                    }
                  }
                ]
              }
            ]
          }
        }),
      { first, last, before, after }
    );
    return result;
  }

  @SkipThrottle()
  @Query(() => PostConnection)
  @UseGuards(GqlJwtAuthGuard)
  async allPostsByUserId(
    @PostAccountEntity() account: Account,
    @Args() { after, before, first, last, minBurnFilter }: PaginationArgs,
    @Args({ name: 'id', type: () => String, nullable: true })
    id: string,
    @Args({
      name: 'orderBy',
      type: () => PostOrder,
      nullable: true
    })
    orderBy: PostOrder
  ) {
    let result;
    if (account.id === _.toSafeInteger(id)) {
      result = await findManyCursorConnection(
        args =>
          this.prisma.post.findMany({
            include: { postAccount: true, comments: true },
            where: {
              AND: [
                {
                  postAccountId: _.toSafeInteger(id)
                },
                {
                  lotusBurnScore: {
                    gte: minBurnFilter ?? 0
                  }
                },
                { pageId: null },
                { tokenId: null }
              ]
            },
            orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined,
            ...args
          }),
        () =>
          this.prisma.post.count({
            where: {
              AND: [
                {
                  postAccountId: _.toSafeInteger(id)
                },
                {
                  lotusBurnScore: {
                    gte: minBurnFilter ?? 0
                  }
                },
                { pageId: null },
                { tokenId: null }
              ]
            }
          }),
        { first, last, before, after }
      );
    } else {
      result = await findManyCursorConnection(
        args =>
          this.prisma.post.findMany({
            include: { postAccount: true, page: false, token: false },
            where: {
              AND: [
                {
                  postAccountId: _.toSafeInteger(id)
                },
                {
                  lotusBurnScore: {
                    gte: minBurnFilter ?? 0
                  }
                },
                { pageId: null },
                { tokenId: null }
              ]
            },
            orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined,
            ...args
          }),
        () =>
          this.prisma.post.count({
            where: {
              AND: [
                {
                  postAccountId: _.toSafeInteger(id)
                },
                {
                  lotusBurnScore: {
                    gte: minBurnFilter ?? 0
                  }
                },
                { pageId: null },
                { tokenId: null }
              ]
            }
          }),
        { first, last, before, after }
      );
    }
    return result;
  }

  @SkipThrottle()
  @Query(() => PostConnection)
  @UseGuards(GqlJwtAuthGuardByPass)
  async allPostsByHashtagId(
    @PostAccountEntity() account: Account,
    @Args() { after, before, first, last, minBurnFilter }: PaginationArgs,
    @Args({ name: 'id', type: () => String, nullable: true })
    hashtagId: string,
    @Args({
      name: 'orderBy',
      type: () => PostOrder,
      nullable: true
    })
    orderBy: PostOrder
  ) {
    const result = await findManyCursorConnection(
      args =>
        this.prisma.post.findMany({
          include: { postAccount: true, comments: true, postHashtags: true },
          where: {
            postHashtags: {
              some: {
                hashtagId: hashtagId
              }
            }
          },
          orderBy: orderBy ? { [orderBy.field]: orderBy.direction } : undefined,
          ...args
        }),
      () =>
        this.prisma.post.count({
          where: {
            postHashtags: {
              some: {
                hashtagId: hashtagId
              }
            }
          }
        }),
      { first, last, before, after }
    );
    return result;
  }

  @Throttle(2, 1)
  @UseGuards(GqlThrottlerGuard)
  @UseGuards(GqlJwtAuthGuard)
  @Mutation(() => Post)
  async createPost(@PostAccountEntity() account: Account, @Args('data') data: CreatePostInput) {
    if (!account) {
      const couldNotFindAccount = await this.i18n.t('post.messages.couldNotFindAccount');
      throw new Error(couldNotFindAccount);
    }

    const { uploadCovers, pageId, htmlContent, tokenPrimaryId, pureContent } = data;

    let uploadDetailIds: any[] = [];

    const promises = uploadCovers.map(async (id: string) => {
      const uploadDetails = await this.prisma.uploadDetail.findFirst({
        where: {
          uploadId: id
        }
      });

      return uploadDetails && uploadDetails.id;
    });

    uploadDetailIds = await Promise.all(promises);

    const postToSave = {
      content: htmlContent,
      postAccount: { connect: { id: account.id } },
      uploadedCovers: {
        connect:
          uploadDetailIds.length > 0
            ? uploadDetailIds.map((uploadDetail: any) => {
                return {
                  id: uploadDetail
                };
              })
            : undefined
      },
      page: {
        connect: pageId ? { id: pageId } : undefined
      },
      token: {
        connect: tokenPrimaryId ? { id: tokenPrimaryId } : undefined
      }
    };

    let createFee: any;
    if (data.createFeeHex) {
      const txData = await this.XPI.RawTransactions.decodeRawTransaction(data.createFeeHex);
      createFee = txData['vout'][0].value;
      if (Number(createFee) < 0) {
        throw new Error('Syntax error. Number cannot be less than or equal to 0');
      }
    }

    const savedPost = await this.prisma.$transaction(async prisma => {
      let txid: string | undefined;
      if (data.createFeeHex) {
        const broadcastResponse = await this.chronik.broadcastTx(data.createFeeHex);
        if (!broadcastResponse) {
          throw new Error('Empty chronik broadcast response');
        }
        txid = broadcastResponse.txid;
      }

      const createdPost = await prisma.post.create({
        data: {
          ...postToSave,
          txid: txid,
          createFee: createFee
        },
        include: {
          page: {
            select: {
              id: true,
              address: true,
              name: true
            }
          },
          token: {
            select: {
              id: true,
              name: true
            }
          },
          postAccount: {
            select: {
              id: true,
              name: true,
              address: true
            }
          }
        }
      });

      return createdPost;
    });

    //Hashtag
    const hashtags = await this.hashtagService.extractAndSave(
      `${process.env.MEILISEARCH_BUCKET}_${HASHTAG}`,
      pureContent,
      savedPost.id
    );

    const indexedPost = {
      id: savedPost.id,
      content: pureContent,
      postAccountName: savedPost.postAccount.name,
      createdAt: savedPost.createdAt,
      updatedAt: savedPost.updatedAt,
      page: {
        id: savedPost.page?.id,
        name: savedPost.page?.name
      },
      token: {
        id: savedPost.token?.id,
        name: savedPost.token?.name
      },
      hashtag: hashtags
    };

    await this.meiliService.add(`${process.env.MEILISEARCH_BUCKET}_${POSTS}`, indexedPost, savedPost.id);

    pubSub.publish('postCreated', { postCreated: savedPost });

    // Notification
    if (pageId && savedPost) {
      const page = await this.prisma.page.findFirst({
        where: {
          id: pageId
        }
      });

      if (!page) {
        const accountNotExistMessage = await this.i18n.t('page.messages.couldNotFindPage');
        throw new VError(accountNotExistMessage);
      }

      const recipient = await this.prisma.account.findFirst({
        where: {
          id: _.toSafeInteger(page.pageAccountId)
        }
      });

      if (!recipient) {
        const accountNotExistMessage = await this.i18n.t('account.messages.accountNotExist');
        throw new VError(accountNotExistMessage);
      }

      const createNotif = {
        senderId: account.id,
        recipientId: Number(page?.pageAccountId),
        notificationTypeId: NOTIFICATION_TYPES.POST_ON_PAGE,
        level: NotificationLevel.INFO,
        url: `/post/${savedPost.id}`,
        additionalData: {
          senderName: account.name,
          senderAddress: account.address,
          pageName: savedPost?.page?.name
        }
      };
      const jobData = {
        notification: createNotif
      };
      createNotif.senderId !== createNotif.recipientId &&
        (await this.notificationService.saveAndDispatchNotification(jobData.notification));
    }

    return savedPost;
  }

  @SkipThrottle()
  @UseGuards(GqlJwtAuthGuard)
  @Mutation(() => Post)
  async updatePost(@PostAccountEntity() account: Account, @Args('data') data: UpdatePostInput) {
    if (!account) {
      const couldNotFindAccount = await this.i18n.t('post.messages.couldNotFindAccount');
      throw new Error(couldNotFindAccount);
    }

    const { id, htmlContent, pureContent } = data;

    const post = await this.prisma.post.findUnique({
      where: {
        id: id
      },
      include: {
        postAccount: {
          select: {
            address: true
          }
        }
      }
    });

    if (post?.postAccount.address !== account.address) {
      const noPermissionToUpdate = await this.i18n.t('post.messages.noPermissionToUpdate');
      throw new Error(noPermissionToUpdate);
    }

    if (post?.lotusBurnScore !== 0) {
      const noPermissionToUpdate = await this.i18n.t('post.messages.noPermissionToUpdate');
      throw new Error(noPermissionToUpdate);
    }

    const updatedPost = await this.prisma.post.update({
      where: {
        id: id
      },
      data: {
        content: htmlContent,
        updatedAt: new Date()
      }
    });

    const indexedPost = {
      id: updatedPost.id,
      content: pureContent,
      updatedAt: updatedPost.updatedAt
    };

    await this.meiliService.update(`${process.env.MEILISEARCH_BUCKET}_${POSTS}`, indexedPost, updatedPost.id);

    pubSub.publish('postUpdated', { postUpdated: updatedPost });
    return updatedPost;
  }

  @SkipThrottle()
  @UseGuards(GqlJwtAuthGuard)
  @Mutation(() => Boolean)
  async repost(@PostAccountEntity() account: Account, @Args('data') data: RepostInput) {
    if (!account) {
      const couldNotFindAccount = await this.i18n.t('post.messages.couldNotFindAccount');
      throw new Error(couldNotFindAccount);
    }

    if (account.id !== data.accountId) {
      const noPermission = await this.i18n.t('account.messages.noPermission');
      throw new Error(noPermission);
    }

    let repostFee: any;
    if (data.txHex) {
      const txData = await this.XPI.RawTransactions.decodeRawTransaction(data.txHex);
      repostFee = txData['vout'][0].value;
      if (Number(repostFee) <= 0) {
        throw new Error('Syntax error. Number cannot be less than or equal to 0');
      }
    }

    const reposted = await this.prisma.$transaction(async prisma => {
      let txid = null;
      if (data.txHex) {
        const broadcastResponse = await this.chronik.broadcastTx(data.txHex).catch(async err => {
          throw new Error('Empty chronik broadcast response');
        });
        txid = broadcastResponse.txid;
      }

      const updatePost = await prisma.post.update({
        where: { id: data.postId },

        data: {
          lastRepostAt: new Date(),
          reposts: {
            create: {
              accountId: account.id,
              repostFee: repostFee,
              txid: txid
            }
          }
        }
      });

      return updatePost;
    });

    return reposted ? true : false;
  }

  @ResolveField('postAccount', () => Account)
  async postAccount(@Parent() post: Post) {
    const account = this.prisma.account.findFirst({
      where: {
        id: post.postAccountId
      }
    });

    return account;
  }

  @ResolveField('totalComments', () => Number)
  async postComments(@Parent() post: Post) {
    const totalComments = await this.prisma.comment.count({
      where: {
        commentToId: post.id
      }
    });

    return totalComments;
  }

  @ResolveField('page', () => Page)
  async page(@Parent() post: Post) {
    if (post.pageId) {
      const page = this.prisma.page.findFirst({
        where: {
          id: post.pageId
        }
      });

      return page;
    }
    return null;
  }

  @ResolveField('token', () => Token)
  async token(@Parent() post: Post) {
    if (post.tokenId) {
      const token = this.prisma.token.findFirst({
        where: {
          id: post.tokenId
        }
      });

      return token;
    }
    return null;
  }

  @ResolveField()
  async uploads(@Parent() post: Post) {
    const uploads = this.prisma.uploadDetail.findMany({
      where: {
        postId: post.id
      },
      include: {
        upload: {
          select: {
            id: true,
            sha: true,
            bucket: true,
            width: true,
            height: true,
            sha800: true,
            sha320: true,
            sha40: true
          }
        }
      }
    });
    return uploads;
  }
}
