export enum Follow {
  Followers = 'followers',
  Followees = 'youFollow',
  FollowingPages = 'followingPages'
}
